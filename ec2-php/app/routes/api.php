<?php

use App\Http\Controllers\DivisionController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\SkillController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/hello', function () {
    return 'Hello World';
});

Route::middleware('api_key_verified', 'add_server_key')->group(function () { // API認証が邪魔なときはコメントアウト
    // 社員系
    Route::prefix('members')->group(function () {
        // 全社員取得
        Route::get('/', [MemberController::class, 'list']);
        // 特定のスキルを持つ社員一覧取得
        Route::get('/search', [MemberController::class, 'search']);
        // スキルごとの社員の平均年齢取得
        Route::get('/skill_average_age', [MemberController::class, 'getSkillAverageAge']);
        // IDで社員取得
        Route::get('/{member_id}', [MemberController::class, 'detail']);
        // 社員作成
        Route::post('/', [MemberController::class, 'create']);
        // 社員にスキルを関連付け
        Route::post('/{member_id}/add_skill', [MemberController::class, 'addSkill']);
        // 社員更新
        Route::put('/{member_id}', [MemberController::class, 'update']);
        // 社員削除
        Route::delete('/{member_id}', [MemberController::class, 'delete']);
    });

    // 事業部系
    Route::prefix('divisions')->group(function () {
        // 全事業部取得
        Route::get('/', [DivisionController::class, 'list']);
        // IDで事業部取得
        Route::get('/{division_id}', [DivisionController::class, 'detail']);
        // 事業部作成
        Route::post('/', [DivisionController::class, 'create']);
        // 事業部更新
        Route::put('/{division_id}', [DivisionController::class, 'update']);
        // 事業部削除
        Route::delete('/{division_id}', [DivisionController::class, 'delete']);
    });

    // スキル系
    Route::prefix('skills')->group(function () {
        // 全スキル取得
        Route::get('/', [SkillController::class, 'list']);
        // IDでスキル取得
        Route::get('/{skill_id}', [SkillController::class, 'detail']);
        // スキル作成
        Route::post('/', [SkillController::class, 'create']);
        // スキル更新
        Route::put('/{skill_id}', [SkillController::class, 'update']);
        // スキル削除
        Route::delete('/{skill_id}', [SkillController::class, 'delete']);
    });
});
