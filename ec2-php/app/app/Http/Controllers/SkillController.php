<?php

namespace App\Http\Controllers;

use App\Http\Requests\SkillCreateRequest;
use App\Http\Requests\SkillDeleteRequest;
use App\Http\Requests\SkillUpdateRequest;
use App\Models\Skill;
use App\Services\SkillService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SkillController extends Controller
{
    private $skillService;

    public function __construct(SkillService $skillService)
    {
        $this->skillService = $skillService;
    }

    /**
     * 全スキル取得
     *
     * @param Request $request
     * @return Collection
     */
    public function list(Request $request): Collection
    {
        return $this->skillService->list();
    }

    /**
     * IDでスキル取得
     *
     * @param Request $request
     * @param integer $skill_id
     * @return Skill
     */
    public function detail(Request $request, int $skill_id): Skill
    {
        return $this->skillService->detail($skill_id);
    }

    /**
     * スキル作成
     *
     * @param SkillCreateRequest $request
     * @return void
     */
    public function create(SkillCreateRequest $request): void
    {
        $this->skillService->create($request->only('name')); // 取得するパラメータを限定
    }

    /**
     * スキル更新
     *
     * @param SkillUpdateRequest $request
     * @param integer $skill_id
     * @return void
     */
    public function update(SkillUpdateRequest $request, int $skill_id): void
    {
        $this->skillService->update($skill_id, $request->only('name')); // 取得するパラメータを限定
    }

    /**
     * スキル削除
     *
     * @param SkillDeleteRequest $request
     * @param integer $skill_id
     * @return void
     */
    public function delete(SkillDeleteRequest $request, int $skill_id): void
    {
        $this->skillService->delete($skill_id);
    }
}
