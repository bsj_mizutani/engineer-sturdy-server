<?php

namespace App\Http\Controllers;

use App\Http\Requests\DivisionCreateRequest;
use App\Http\Requests\DivisionDeleteRequest;
use App\Http\Requests\DivisionUpdateRequest;
use App\Models\Division;
use App\Services\DivisionService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class DivisionController extends Controller
{
    private $divisionService;

    public function __construct(DivisionService $divisionService)
    {
        $this->divisionService = $divisionService;
    }

    /**
     * 全事業部取得
     *
     * @param Request $request
     * @return Collection
     */
    public function list(Request $request): Collection
    {
        return $this->divisionService->list();
    }

    /**
     * IDで事業部取得
     *
     * @param Request $request
     * @param integer $division_id
     * @return Division
     */
    public function detail(Request $request, int $division_id): Division
    {
        return $this->divisionService->detail($division_id);
    }

    /**
     * 事業部作成
     *
     * @param DivisionCreateRequest $request
     * @return void
     */
    public function create(DivisionCreateRequest $request): void
    {
        $this->divisionService->create($request->only('name')); // 取得するパラメータを限定
    }

    /**
     * 事業部更新
     *
     * @param DivisionUpdateRequest $request
     * @param integer $division_id
     * @return void
     */
    public function update(DivisionUpdateRequest $request, int $division_id): void
    {
        $this->divisionService->update($division_id, $request->only('name')); // 取得するパラメータを限定
    }

    /**
     * 事業部削除
     *
     * @param DivisionDeleteRequest $request
     * @param integer $division_id
     * @return void
     */
    public function delete(DivisionDeleteRequest $request, int $division_id): void
    {
        $this->divisionService->delete($division_id);
    }
}
