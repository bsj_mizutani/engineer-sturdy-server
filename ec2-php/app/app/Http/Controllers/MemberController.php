<?php

namespace App\Http\Controllers;

use App\Http\Requests\MemberAddSkillRequest;
use App\Http\Requests\MemberCreateRequest;
use App\Http\Requests\MemberDeleteRequest;
use App\Http\Requests\MemberSearchRequest;
use App\Http\Requests\MemberUpdateRequest;
use App\Models\Member;
use App\Services\MemberService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class MemberController extends Controller
{
    private $memberService;

    public function __construct(MemberService $memberService)
    {
        $this->memberService = $memberService;
    }

    /**
     * 全社員取得
     *
     * @param Request $request
     * @return array
     */
    public function list(Request $request): array
    {
        $per_page = $request->query('per_page', 25);
        return $this->memberService->list($per_page);
    }

    /**
     * IDで社員取得
     *
     * @param Request $request
     * @param integer $member_id
     * @return Member
     */
    public function detail(Request $request, int $member_id): Member
    {
        return $this->memberService->detail($member_id);
    }

    /**
     * 社員作成
     *
     * @param MemberCreateRequest $request
     * @return void
     */
    public function create(MemberCreateRequest $request): void
    {
        $this->memberService->create($request->only('division_id', 'name', 'age')); // 取得するパラメータを限定
    }

    /**
     * 社員更新
     *
     * @param MemberUpdateRequest $request
     * @param integer $member_id
     * @return void
     */
    public function update(MemberUpdateRequest $request, int $member_id): void
    {
        $this->memberService->update($member_id, $request->only('division_id', 'name', 'age')); // 取得するパラメータを限定
    }

    /**
     * 社員削除
     *
     * @param MemberDeleteRequest $request
     * @param integer $member_id
     * @return void
     */
    public function delete(MemberDeleteRequest $request, int $member_id): void
    {
        $this->memberService->delete($member_id);
    }

    /**
     * 特定条件の社員一覧取得
     *
     * @param MemberSearchRequest $request
     * @return Collection
     */
    public function search(MemberSearchRequest $request): Collection
    {
        return $this->memberService->search($request->only('skill_id', 'division_id', 'name'));
    }

    /**
     * 社員にスキルを関連付け
     *
     * @param MemberAddSkillRequest $request
     * @return void
     */
    public function addSkill(MemberAddSkillRequest $request, int $member_id): void
    {
        $this->memberService->addSkill($member_id, $request->only('skill_id'));
    }

    /**
     * スキルごとの社員の平均年齢取得
     *
     * @param Request $request
     * @return array
     */
    public function getSkillAverageAge(Request $request): array
    {
        return $this->memberService->getSkillAverageAge2();
    }
}
