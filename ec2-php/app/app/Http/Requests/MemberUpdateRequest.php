<?php

namespace App\Http\Requests;

use App\Rules\ExistingDivisionIdRule;
use App\Rules\ExistingMemberIdRule;

class MemberUpdateRequest extends ApiRequest
{
    private $existing_member_id_rule;
    private $existing_division_id_rule;

    public function __construct(
        ExistingMemberIdRule $existing_member_id_rule,
        ExistingDivisionIdRule $existing_division_id_rule
    ) {
        $this->existing_member_id_rule = $existing_member_id_rule;
        $this->existing_division_id_rule = $existing_division_id_rule;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'member_id' => [
                'required',
                'integer',
                $this->existing_member_id_rule,
            ],
            'name' => ['required', 'string', 'max:255'],
            'division_id' => [
                'required',
                'integer',
                $this->existing_division_id_rule,
            ],
        ];
    }

    public function validationData()
    {
        return array_merge($this->request->all(), [
            'member_id' => $this->member_id
        ]);
    }
}
