<?php

namespace App\Http\Requests;

use App\Rules\ExistingMemberIdRule;
use App\Rules\ExistingSkillIdRule;
use App\Rules\NotExistingMemberAndSkillIdRule;

class MemberAddSkillRequest extends ApiRequest
{
    private $existing_member_id_rule;
    private $existing_skill_id_rule;
    private $not_existing_member_and_skill_rule;

    public function __construct(
        ExistingMemberIdRule $existing_member_id_rule,
        ExistingSkillIdRule $existing_skill_id_rule,
        NotExistingMemberAndSkillIdRule $not_existing_member_and_skill_rule
    ) {
        $this->existing_member_id_rule = $existing_member_id_rule;
        $this->existing_skill_id_rule = $existing_skill_id_rule;
        $this->not_existing_member_and_skill_rule = $not_existing_member_and_skill_rule;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'member_id' => [
                'required',
                'integer',
                $this->existing_member_id_rule
            ],
            'skill_id' => [
                'required',
                'integer',
                $this->existing_skill_id_rule,
                $this->not_existing_member_and_skill_rule
            ],
        ];
    }

    public function validationData()
    {
        return array_merge($this->request->all(), [
            'member_id' => $this->member_id
        ]);
    }
}
