<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;

abstract class ApiRequest extends FormRequest
{
    /**
     * 認証済みかどうかを指定
     * （ここがfalseだと未認証エラーとなるが、今回は認証しないので常にtrue）
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * バリデーション失敗時の処理
     * （Laravelではデフォルトでは422が発生するとトップページへリダイレクトしてしまう）
     *
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        $data = [
            'message' => __('The given data was invalid.'),
            'errors' => $validator->errors()->toArray(),
        ];

        throw new HttpResponseException(response()->json($data, Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
