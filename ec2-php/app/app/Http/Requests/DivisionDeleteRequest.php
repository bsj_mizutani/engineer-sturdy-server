<?php

namespace App\Http\Requests;

use App\Rules\DeleteDivisionIdRule;
use App\Rules\ExistingDivisionIdRule;
use Illuminate\Validation\Rule;

class DivisionDeleteRequest extends ApiRequest
{
    private $existing_division_id_rule;
    private $delete_division_id_rule;

    public function __construct(
        ExistingDivisionIdRule $existing_division_id_rule,
        DeleteDivisionIdRule $delete_division_id_rule
    ) {
        $this->existing_division_id_rule = $existing_division_id_rule;
        $this->delete_division_id_rule = $delete_division_id_rule;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'division_id' => [
                'required',
                'integer',
                $this->existing_division_id_rule,
                $this->delete_division_id_rule
            ]
        ];
    }

    public function validationData()
    {
        return array_merge($this->request->all(), [
            'division_id' => $this->division_id
        ]);
    }
}
