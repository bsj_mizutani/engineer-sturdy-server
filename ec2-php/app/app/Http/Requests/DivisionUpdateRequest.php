<?php

namespace App\Http\Requests;

use App\Rules\ExistingDivisionIdRule;

class DivisionUpdateRequest extends ApiRequest
{
    private $existing_division_id_rule;

    public function __construct(
        ExistingDivisionIdRule $existing_division_id_rule
    ) {
        $this->existing_division_id_rule = $existing_division_id_rule;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'division_id' => [
                'required',
                'integer',
                $this->existing_division_id_rule,
            ],
            'name' => ['required', 'string', 'max:255']
        ];
    }

    /**
     * バリデーション対象を指定
     * （division_idはパスパラメータなので、デフォルトでバリデーション対象にならない）
     *
     * @return void
     */
    public function validationData()
    {
        return array_merge($this->request->all(), [
            'division_id' => $this->division_id
        ]);
    }
}
