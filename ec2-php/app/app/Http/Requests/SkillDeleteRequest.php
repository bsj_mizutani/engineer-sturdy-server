<?php

namespace App\Http\Requests;

use App\Rules\ExistingSkillIdRule;

class SkillDeleteRequest extends ApiRequest
{
    private $existing_skill_id_rule;

    public function __construct(
        ExistingSkillIdRule $existing_skill_id_rule
    ) {
        $this->existing_skill_id_rule = $existing_skill_id_rule;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'skill_id' => [
                'required',
                'integer',
                $this->existing_skill_id_rule,
            ],
        ];
    }

    public function validationData()
    {
        return array_merge($this->request->all(), [
            'skill_id' => $this->skill_id
        ]);
    }
}
