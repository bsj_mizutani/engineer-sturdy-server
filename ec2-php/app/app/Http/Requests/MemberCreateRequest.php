<?php

namespace App\Http\Requests;

use App\Rules\ExistingDivisionIdRule;

class MemberCreateRequest extends ApiRequest
{
    private $existing_division_id_rule;

    public function __construct(
        ExistingDivisionIdRule $existing_division_id_rule
    ) {
        $this->existing_division_id_rule = $existing_division_id_rule;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'division_id' => [
                'required',
                'integer',
                $this->existing_division_id_rule,
            ],
        ];
    }
}
