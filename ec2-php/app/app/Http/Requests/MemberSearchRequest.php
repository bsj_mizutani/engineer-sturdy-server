<?php

namespace App\Http\Requests;

use App\Rules\ExistingDivisionIdRule;
use App\Rules\ExistingSkillIdRule;

class MemberSearchRequest extends ApiRequest
{
    private $existing_skill_id_rule;
    private $existing_division_id_rule;

    public function __construct(
        ExistingSkillIdRule $existing_skill_id_rule,
        ExistingDivisionIdRule $existing_division_id_rule
    ) {
        $this->existing_skill_id_rule = $existing_skill_id_rule;
        $this->existing_division_id_rule = $existing_division_id_rule;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'skill_id' => [
                'nullable',
                'integer',
                $this->existing_skill_id_rule,
            ],
            'division_id' => [
                'nullable',
                'integer',
                $this->existing_division_id_rule,
            ],
            'name' => ['nullable', 'string'],
        ];
    }
}
