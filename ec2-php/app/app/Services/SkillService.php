<?php

namespace App\Services;

use App\Models\Skill;
use App\Repositories\SkillRepository;
use Illuminate\Support\Collection;

class SkillService
{
    private $skillRepository;

    public function __construct(SkillRepository $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }

    /**
     * 全スキル取得
     *
     * @return Collection
     */
    public function list(): Collection
    {
        return $this->skillRepository->all();
    }

    /**
     * IDでスキル取得
     *
     * @param integer $skill_id
     * @return Skill
     */
    public function detail(int $skill_id): Skill
    {
        return $this->skillRepository->find($skill_id);
    }

    /**
     * スキル作成
     *
     * @param array $params
     * @return void
     */
    public function create(array $params): void
    {
        $this->skillRepository->create($params);
    }

    /**
     * スキル更新
     *
     * @param integer $skill_id
     * @param array $params
     * @return void
     */
    public function update(int $skill_id, array $params): void
    {
        $this->skillRepository->update($skill_id, $params);
    }

    /**
     * スキル削除
     *
     * @param integer $skill_id
     * @return void
     */
    public function delete(int $skill_id): void
    {
        $this->skillRepository->delete($skill_id);
    }
}
