<?php

namespace App\Services;

use App\Models\Member;
use App\Repositories\MemberRepository;
use App\Repositories\SkillRepository;
use App\Repositories\SkillSetRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class MemberService
{
    private $memberRepository;
    private $skillSetRepository;
    private $skillRepository;

    public function __construct(
        MemberRepository $memberRepository,
        SkillSetRepository $skillSetRepository,
        SkillRepository $skillRepository
    ) {
        $this->memberRepository = $memberRepository;
        $this->skillSetRepository = $skillSetRepository;
        $this->skillRepository = $skillRepository;
    }

    /**
     * 全社員取得
     *
     * @param integer $per_page
     * @return array
     */
    public function list(int $per_page): array
    {
        return $this->memberRepository->getAllWithPagination($per_page)->toArray();
    }

    /**
     * IDで社員取得
     *
     * @param integer $member_id
     * @return Member
     */
    public function detail(int $member_id): Member
    {
        return $this->memberRepository->find($member_id);
    }

    /**
     * 社員作成
     *
     * @param array $params
     * @return void
     */
    public function create(array $params): void
    {
        $this->memberRepository->create($params);
    }

    /**
     * 社員更新
     *
     * @param integer $member_id
     * @param array $params
     * @return void
     */
    public function update(int $member_id, array $params): void
    {
        $this->memberRepository->update($member_id, $params);
    }

    /**
     * 社員削除
     *
     * @param integer $member_id
     * @return void
     */
    public function delete(int $member_id): void
    {
        $this->memberRepository->delete($member_id);
    }

    /**
     * 特定条件の社員一覧取得
     *
     * @param array $params
     * @return Collection
     */
    public function search(array $params): Collection
    {
        $skill_id = $params['skill_id'] ?? null;
        $division_id = $params['division_id'] ?? null;
        $name = $params['name'] ?? null;
        return $this->memberRepository->search($skill_id, $division_id, $name);
    }

    /**
     * 社員にスキルを関連付け
     *
     * @param integer $member_id
     * @param array $params
     * @return void
     */
    public function addSkill(int $member_id, array $params): void
    {
        $skill_id = $params['skill_id'];
        $this->skillSetRepository->create([
            'member_id' => $member_id,
            'skill_id' => $skill_id
        ]);
    }

    /**
     * スキルごとの社員の平均年齢取得
     * （低速版）
     *
     * @return array
     */
    public function getSkillAverageAge(): array
    {
        $all_skills = $this->skillRepository->all();
        $results = [];
        foreach ($all_skills as $skill) {
            $count_and_sum = $this->memberRepository->getTargetSkillCountAndAvgAge($skill->id);
            $results[] = [
                'name' => $skill->name,
                'count' => $count_and_sum['count'],
                'average_age' => $count_and_sum['avg']
            ];
        }
        return $results;
    }

    /**
     * スキルごとの社員の平均年齢取得
     * （改善版）
     *
     * @return array
     */
    public function getSkillAverageAge2(): array
    {
        return $this->memberRepository->getSkillAverageAge();
    }

    public function getMemberAndSkill1(int $max_member_id): array
    {
        $start_time = microtime(true);
        $results = [];
        $members = $this->memberRepository->getByMaxId($max_member_id);
        foreach ($members as $member) {
            $skills = $this->skillRepository->getByMemberId($member->id);
            foreach ($skills as $skill) {
                $results[] = [
                    'member_name' => $member->name,
                    'skill_name' => $skill->name
                ];
            }
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        // return $results;
        return [];
    }

    public function getMemberAndSkill2(int $max_member_id): array
    {
        $start_time = microtime(true);
        $results = [];
        $members = $this->memberRepository->getByMaxId($max_member_id);
        $member_ids = $members->pluck('id')->toArray();
        $member_and_skills = $this->skillRepository->getByMemberIds($member_ids);
        foreach ($members as $member) {
            $skills = $member_and_skills->where('member_id', $member->id);
            foreach ($skills as $skill) {
                $results[] = [
                    'member_name' => $member->name,
                    'skill_name' => $skill->name
                ];
            }
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        // return $results;
        return [];
    }

    public function getMemberAndSkill3(int $max_member_id): array
    {
        $start_time = microtime(true);
        $results = [];
        $members = $this->memberRepository->getByMaxId($max_member_id);
        $member_ids = $members->pluck('id')->toArray();
        $member_and_skills = $this->skillRepository->getByMemberIds($member_ids);
        $member_and_skill_array = $member_and_skills->groupBy('member_id')->toArray();
        foreach ($members as $member) {
            $skills = $member_and_skill_array[$member->id];
            foreach ($skills as $skill) {
                $results[] = [
                    'member_name' => $member->name,
                    'skill_name' => $skill['name']
                ];
            }
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        // return $results;
        return [];
    }

    public function getMemberAndSkill4(int $max_member_id): array
    {
        $start_time = microtime(true);
        $results = $this->skillSetRepository->getMemberNameAndSkillNameByMaxMemberId($max_member_id);
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        return $results;
        // return [];
    }

    public function chunkTest1(int $skill_id): void
    {
        $member_ids = [];
        // テストなので適当に範囲を作成
        for ($i = 1; $i <= 70000; $i++) {
            $member_ids[] = $i;
        };
        try {
            // ターミナル上でエラーが省略されることを防ぐために、ひとまずエラーはログとして残るようにする
            $skill_count = $this->skillSetRepository->countSkillByMemberIdsAndSkillId($member_ids, $skill_id);
            print($skill_count . "\n");
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function chunkTest2(int $skill_id): void
    {
        $member_ids = [];
        for ($i = 1; $i <= 70000; $i++) {
            $member_ids[] = $i;
        };
        $chunked_member_ids = array_chunk($member_ids, 5000);
        $skill_count = 0;
        foreach ($chunked_member_ids as $one_chunked_member_ids) {
            $skill_count += $this->skillSetRepository->countSkillByMemberIdsAndSkillId($one_chunked_member_ids, $skill_id);
        }
        print($skill_count . "\n");
    }
}
