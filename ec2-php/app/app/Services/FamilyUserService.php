<?php

namespace App\Services;

use App\Repositories\FamilyUserRepository;
use Illuminate\Support\Facades\DB;

class FamilyUserService
{
    private $familyUserRepository;

    public function __construct(FamilyUserRepository $familyUserRepository)
    {
        $this->familyUserRepository = $familyUserRepository;
    }

    /**
     * IDからメインユーザへの変換テーブル作成処理
     * （低速版）
     *
     * @return array
     */
    public function getUserMappingList1(): array
    {
        $start_time = microtime(true);
        $results = [];
        $users = $this->familyUserRepository->all();
        foreach ($users as $user) {
            if (is_null($user->family_id)) {
                $results[$user->id] = $user->id;
                continue;
            }
            $mainUser = $this->familyUserRepository->findMainUser($user->family_id);
            if (is_null($mainUser)) {
                continue;
            }
            $results[$user->id] = $mainUser->id;
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        return $results;
    }

    /**
     *  IDからメインユーザへの変換テーブル作成処理
     * （改善版）
     *
     * @return array
     */
    public function getUserMappingList2(): array
    {
        $start_time = microtime(true);
        $results = [];
        $users = $this->familyUserRepository->getAllOrderByFamilyAndMain();
        $main_user_id = 0;
        foreach ($users as $user) {
            $user_id = $user->id;
            if (is_null($user->family_id)) {
                // 家族に紐付いていない場合はそのままのユーザID
                $results[$user_id] = $user_id;
            } elseif ($user->is_main === 1) {
                // メインユーザの場合もそのままのユーザIDで、そのIDを一時保持しておく
                $results[$user_id] = $user_id;
                $main_user_id = $user_id;
            } else {
                // サブユーザの場合はメインユーザID
                // （あらかじめfamily_id, is_mainでソートしているため狂うことはない）
                $results[$user_id] = $main_user_id;
            }
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        return $results;
    }

    /**
     *  IDからメインユーザへの変換テーブル作成処理
     * （Aさん改善版）
     *
     * @return Array
     */
    public function getUserMappingListA(): Array
    {
        $start_time = microtime(true);
        $results = [];
        $values = $this->familyUserRepository->getIdAndMainUserIdsA();
        foreach ($values as $value) {
            $id = $value->id;
            $main_user_id = $value->main_user_id;
            $results[$id] = $main_user_id;
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        return $results;
    }

    /**
     * IDからメインユーザへの変換テーブル作成処理
     * （Bさん改善版）
     *
     * @return array
     */
    public function getUserMappingListB(): array
    {
        $start_time = microtime(true);
        $results = [];
        $mainUsers = $this->familyUserRepository->getMainUsersB();
        $notMainUsers = $this->familyUserRepository->getNotMainUsersB();
        $mainFamilyIds = [];
        foreach ($mainUsers as $mainUser) {
            $id = $mainUser['id'];
            $family_id = $mainUser['family_id'];

            if ($family_id === null) {
                $family_id = $id;
            }

            $results[$id] = $id;
            $mainFamilyIds[$family_id] = $id;
        }

        foreach ($notMainUsers as $notMainUser) {
            $id = $notMainUser['id'];
            $family_id = $notMainUser['family_id'];
            if (array_key_exists($family_id, $mainFamilyIds)) {
                $results[$id] = $mainFamilyIds[$family_id];
            }
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        return $results;
    }

    /**
     * IDからメインユーザへの変換テーブル作成処理
     * （Cさん改善版）
     *
     * @return array
     */
    public function getUserMappingListC(): array
    {
        $start_time = microtime(true);
        $results=[];
        $list = DB::select("
            SELECT t1.id, IFNULL(t2.id,t1.id) as main_id 
            FROM laravel.family_users t1 
            LEFT JOIN laravel.family_users t2 ON t1.family_id = t2.family_id
            where 
                (t1.is_main = 1 AND t2.is_main = 1) 
            OR 
                (t1.family_id is NULL)
            OR
                (t1.is_main = 0 AND t2.is_main = 1)
            order by t1.id;
        ");
        forEach($list as $row){
            $results[$row->id] = $row->main_id;
        }
        $end_time = microtime(true);
        print('time: ' . ($end_time - $start_time) . "\n");
        return $results;
    }
}
