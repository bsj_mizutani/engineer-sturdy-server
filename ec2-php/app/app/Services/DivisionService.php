<?php

namespace App\Services;

use App\Models\Division;
use App\Repositories\DivisionRepository;
use Illuminate\Support\Collection;

class DivisionService
{
    private $divisionRepository;

    public function __construct(DivisionRepository $divisionRepository)
    {
        $this->divisionRepository = $divisionRepository;
    }

    /**
     * 全事業部取得
     *
     * @return Collection
     */
    public function list(): Collection
    {
        return $this->divisionRepository->all();
    }

    /**
     * IDで事業部取得
     *
     * @param integer $division_id
     * @return Division
     */
    public function detail(int $division_id): Division
    {
        return $this->divisionRepository->find($division_id);
    }

    /**
     * 事業部作成
     *
     * @param array $params
     * @return void
     */
    public function create(array $params): void
    {
        $this->divisionRepository->create($params);
    }

    /**
     * 事業部更新
     *
     * @param integer $division_id
     * @param array $params
     * @return void
     */
    public function update(int $division_id, array $params): void
    {
        $this->divisionRepository->update($division_id, $params);
    }

    /**
     * 事業部削除
     *
     * @param integer $division_id
     * @return void
     */
    public function delete(int $division_id): void
    {
        $this->divisionRepository->delete($division_id);
    }
}
