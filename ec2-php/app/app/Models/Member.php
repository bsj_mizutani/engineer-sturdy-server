<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;

    /**
     * 操作できないカラム
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * レスポンスに含めないカラム
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Division に対して 多対1 の関係
     *
     * @return void
     */
    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    /**
     * SkillSet に対して 1対多 の関係
     *
     * @return void
     */
    public function skillSet()
    {
        return $this->hasMany(SkillSet::class);
    }

    /**
     * Skill に対して 多対多の関係
     *
     * @return void
     */
    public function skills()
    {
        return $this->belongsToMany('App\Models\Skill', 'skill_set', 'member_id', 'skill_id');
    }
}
