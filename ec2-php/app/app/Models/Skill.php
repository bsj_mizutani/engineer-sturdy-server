<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
    use SoftDeletes;

    /**
     * 操作できないカラム
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * レスポンスに含めないカラム
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * SkillSet に対して 1対多 の関係
     *
     * @return void
     */
    public function skillSet()
    {
        return $this->hasMany(SkillSet::class);
    }

    /**
     * Member に対して 多対多の関係
     *
     * @return void
     */
    public function members()
    {
        return $this->belongsToMany('App\Models\Member', 'skill_set', 'skill_id', 'member_id');
    }
}
