<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkillSet extends Model
{
    use SoftDeletes;

    /**
     * 関連するテーブル
     * （指定しないとテーブル名をskill_setsと解釈してしまう）
     *
     * @var string
     */
    protected $table = 'skill_set';

    /**
     * 操作できないカラム
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * レスポンスに含めないカラム
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Member に対して 1 の関係
     *
     * @return void
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    /**
     * Skill に対して 1 の関係
     *
     * @return void
     */
    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }
}
