<?php

namespace App\Rules;

use App\Repositories\MemberRepository;
use Illuminate\Contracts\Validation\Rule;

class DeleteDivisionIdRule implements Rule
{
    private $r_member;

    public function __construct(
        MemberRepository $r_member
    ) {
        $this->r_member = $r_member;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->r_member->getByDivisionId((int) $value)->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '所属する社員が存在するため事業部を削除することができません。';
    }
}
