<?php

namespace App\Rules;

use App\Repositories\DivisionRepository;
use Illuminate\Contracts\Validation\Rule;

class ExistingDivisionIdRule implements Rule
{
    private $r_division;

    public function __construct(
        DivisionRepository $r_division
    ) {
        $this->r_division = $r_division;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !is_null($this->r_division->find((int) $value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '指定された事業部は存在しません。';
    }
}
