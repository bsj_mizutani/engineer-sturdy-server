<?php

namespace App\Rules;

use App\Repositories\MemberRepository;
use Illuminate\Contracts\Validation\Rule;

class ExistingMemberIdRule implements Rule
{
    private $r_member;

    public function __construct(
        MemberRepository $r_member
    ) {
        $this->r_member = $r_member;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !is_null($this->r_member->find((int) $value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '指定された社員は存在しません。';
    }
}
