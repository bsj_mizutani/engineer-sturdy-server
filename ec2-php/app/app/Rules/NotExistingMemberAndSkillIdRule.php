<?php

namespace App\Rules;

use App\Repositories\SkillSetRepository;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class NotExistingMemberAndSkillIdRule implements Rule
{
    private $member_id;
    private $r_skill_set;

    public function __construct(
        Request $request,
        SkillSetRepository $r_skill_set
    ) {
        $this->member_id = $request->member_id;
        $this->r_skill_set = $r_skill_set;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return is_null($this->r_skill_set->findByMemberIdAndSkillId($this->member_id, (int) $value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '指定された社員とスキルの組み合わせはすでに存在します。';
    }
}
