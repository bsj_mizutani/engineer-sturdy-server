<?php

namespace App\Rules;

use App\Repositories\SkillRepository;
use Illuminate\Contracts\Validation\Rule;

class ExistingSkillIdRule implements Rule
{
    private $r_skill;

    public function __construct(
        SkillRepository $r_skill
    ) {
        $this->r_skill = $r_skill;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !is_null($this->r_skill->find((int) $value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '指定されスキルは存在しません。';
    }
}
