<?php

namespace App\Repositories;

use App\Models\SkillSet;

class SkillSetRepository extends BaseRepository
{
    public function __construct(SkillSet $resource)
    {
        $this->resource = $resource;
    }

    /**
     * 社員IDとスキルIDをもとにデータを取得
     *
     * @param integer $member_id
     * @param integer $skill_id
     * @return SkillSet|null
     */
    public function findByMemberIdAndSkillId(int $member_id, int $skill_id): ?SkillSet
    {
        return $this->resource
            ->where('member_id', $member_id)
            ->where('skill_id', $skill_id)
            ->first();
    }

    /**
     * 指定された社員ID以下の社員名と、その社員が持つスキル名を取得
     *
     * @param integer $max_member_id
     * @return array
     */
    public function getMemberNameAndSkillNameByMaxMemberId(int $max_member_id): array
    {
        return $this->resource
            ->select('members.name as member_name', 'skills.name as skill_name')
            ->join('members', 'members.id', '=', 'skill_set.member_id')
            ->join('skills', 'skills.id', '=', 'skill_set.skill_id')
            ->where('member_id', '<=', $max_member_id)
            ->get()
            ->toArray(); // 自身のモデルそのまま以外を返す場合は配列にする
    }

    /**
     * 指定された社員IDの中で、指定されたスキルの数を集計
     *
     * @param array $member_ids
     * @param integer $skill_id
     * @return integer
     */
    public function countSkillByMemberIdsAndSkillId(array $member_ids, int $skill_id): int
    {
        return $this->resource
            ->whereIn('member_id', $member_ids)
            ->where('skill_id', $skill_id)
            ->count();
    }
}
