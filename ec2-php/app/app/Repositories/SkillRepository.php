<?php

namespace App\Repositories;

use App\Models\Skill;
use Illuminate\Support\Collection;

class SkillRepository extends BaseRepository
{
    public function __construct(Skill $resource)
    {
        $this->resource = $resource;
    }

    public function getByMemberId(int $member_id): Collection
    {
        return $this->resource
            ->select('skills.*')
            ->join('skill_set', 'skill_set.skill_id', '=', 'skills.id')
            ->where('skill_set.member_id', $member_id)
            ->get();
    }

    public function getByMemberIds(array $member_ids): Collection
    {
        return $this->resource
            ->select('skill_set.member_id', 'skills.*')
            ->join('skill_set', 'skill_set.skill_id', '=', 'skills.id')
            ->whereIn('skill_set.member_id', $member_ids)
            ->get();
    }
}
