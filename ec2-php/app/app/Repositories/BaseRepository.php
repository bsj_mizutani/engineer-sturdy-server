<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BaseRepository
{
    protected $resource = null;

    /**
     * 指定されたパラメータでデータを作成
     *
     * @param array $params
     * @return Model 作成したレコード
     */
    public function create(array $params): Model
    {
        return $this->resource->create($params);
    }

    /**
     * 全データを取得
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->resource->all();
    }

    /**
     * IDを指定してデータを取得
     *
     * @param integer $id
     * @return Model|null
     */
    public function find(int $id): ?Model
    {
        return $this->resource->where('id', $id)->first();
    }

    /**
     * 指定されたパラメータでデータを更新
     *
     * @param integer $id
     * @param array $params
     * @return integer 影響を与えたレコード数
     */
    public function update(int $id, array $params): int
    {
        return $this->resource->where('id', $id)->update($params);
    }

    /**
     * IDを指定してデータを削除
     *
     * @param integer $id
     * @return integer 影響を与えたレコード数
     */
    public function delete(int $id): int
    {
        return $this->resource->where('id', $id)->delete();
    }
}
