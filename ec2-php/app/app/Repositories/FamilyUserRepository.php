<?php

namespace App\Repositories;

use App\Models\FamilyUser;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class FamilyUserRepository extends BaseRepository
{
    public function __construct(FamilyUser $resource)
    {
        $this->resource = $resource;
    }

    /**
     * ファミリーIDからメインユーザ取得
     *
     * @param integer $family_id
     * @return FamilyUser|null
     */
    public function findMainUser(int $family_id): ?FamilyUser
    {
        return $this->resource
            ->where('family_id', $family_id)
            ->where('is_main', 1)
            ->first();
    }

    /**
     * 全データをファミリーID昇順、メインユーザ降順にソートして取得
     *
     * @return Collection
     */
    public function getAllOrderByFamilyAndMain(): Collection
    {
        return $this->resource
            ->orderBy('family_id', 'asc')
            ->orderBy('is_main', 'desc')
            ->get();
    }

    /**
     * IDとメインユーザIDの組み合わせを取得
     * （getUserMappingListAから呼ばれる）
     *
     * @return Collection
     */
    public function getIdAndMainUserIdsA(): Collection
    {
        //is_mainが1の行のfamily_idを基準に自己結合して、family_idがnullの場合はt2のidがnullになるためidを入れる。
        return   $this->resource
            ->where('family_users.is_main', 1)
            ->select('family_users.id as main_user_id', DB::raw('COALESCE(t2.id, family_users.id) as id'))
            ->leftJoin('family_users as t2', 't2.family_id', '=', 'family_users.family_id')
            ->get();
    }

    /**
     * メインユーザを取得
     * （getUserMappingListBから呼ばれる）
     *
     * @return array
     */
    public function getMainUsersB(): array
    {
        return $this->resource
            ->select('family_users.id as id', 'family_users.family_id as family_id')
            ->where('is_main', 1)
            ->get()
            ->toArray(); 
    }

    /**
     * メインではないユーザを取得
     * （getUserMappingListBから呼ばれる）
     *
     * @return array
     */
    public function getNotMainUsersB(): array
    {
        return $this->resource
            ->select('family_users.id as id', 'family_users.family_id as family_id')
            ->where('is_main', 0)
            ->get()
            ->toArray();
    }
}
