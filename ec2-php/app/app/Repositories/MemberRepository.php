<?php

namespace App\Repositories;

use App\Models\Member;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MemberRepository extends BaseRepository
{
    public function __construct(Member $resource)
    {
        $this->resource = $resource;
    }

    /**
     * スキルIDを元に社員一覧を取得
     *
     * @param integer $skill_id
     * @return Collection
     */
    public function getBySkillId(int $skill_id): Collection
    {
        return  $this->resource
            ->select('members.*')
            ->join('skill_set', 'skill_set.member_id', '=', 'members.id')
            ->where('skill_set.skill_id', $skill_id)
            ->get();
        // return \App\Models\Skill::find($skill_id)->members()->get();
    }

    /**
     * 事業部IDをもとに社員一覧を取得
     *
     * @param integer $division_id
     * @return Collection
     */
    public function getByDivisionId(int $division_id): Collection
    {
        return  $this->resource
            ->where('division_id', $division_id)
            ->get();
    }

    /**
     * スキルID、事業部ID、氏名による検索
     *
     * @param integer|null $skill_id
     * @param integer|null $division_id
     * @param string|null $name
     * @return Collection
     */
    public function search(?int $skill_id, ?int $division_id, ?string $name): Collection
    {
        $query = $this->resource
            ->select('members.*');

        if (!is_null($skill_id)) {
            // スキルIDが条件に指定されている場合、検索条件を追加
            $query = $query
                ->join(
                    'skill_set',
                    'skill_set.member_id',
                    '=',
                    'members.id'
                )
                ->where('skill_set.skill_id', $skill_id)
                ->whereNull('skill_set.deleted_at');
        }

        if (!is_null($division_id)) {
            // 事業部IDが条件に指定されている場合、検索条件を追加
            $query = $query->where('members.division_id', $division_id);
        }

        if (!is_null($name)) {
            // 事業部IDが条件に指定されている場合、検索条件を追加
            // 用途を考えると、完全一致ではなくLIKE検索がふさわしい
            $query = $query->where('members.name', 'like', "%{$name}%");
        }
        return $query->get();
    }

    /**
     * 指定されたID以下のデータを取得
     *
     * @param integer $max_id
     * @return Collection
     */
    public function getByMaxId(int $max_id): Collection
    {
        return $this->resource
            ->where('id', '<=', $max_id)
            ->get();
    }

    /**
     * ページングありで全データを取得
     *
     * @param integer $per_page
     * @return LengthAwarePaginator
     */
    public function getAllWithPagination(int $per_page): LengthAwarePaginator
    {
        return $this->resource
            ->paginate($per_page);
        // ->getCollection(); // データの中身だけで良い場合はこちらを追加
    }

    /**
     * 指定されたスキルを持つ社員の総数と合計年齢を取得
     *
     * @param integer $skill_id
     * @return array
     */
    public function getTargetSkillCountAndAvgAge(int $skill_id): array
    {
        return $this->resource
            ->selectRaw('count(*) as count, avg(age) as avg')
            ->whereExists(function (Builder $query) use ($skill_id) {
                $query->select(DB::raw(1))
                    ->from('skill_set')
                    ->whereColumn('skill_set.member_id', 'members.id')
                    ->where('skill_set.skill_id', $skill_id);
            })
            ->first()
            ->toArray();
    }

    /**
     * スキルごとの社員の総数と平均年齢をまとめて取得
     *
     * @return array
     */
    public function getSkillAverageAge(): array
    {
        return $this->resource
            ->selectRaw('skills.name, count(*) as count, avg(age) as avg')
            ->join('skill_set', 'skill_set.member_id', '=', 'members.id')
            ->join('skills', 'skills.id', '=', 'skill_set.skill_id')
            ->groupBy('skill_set.skill_id')
            ->orderBy('skill_set.skill_id', 'asc')
            ->get()
            ->toArray();
    }
}
