<?php

return [
    'api_key' => env('API_KEY'),
    'server_key' => env('SERVER_KEY'),
];
