<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    private $data = [];

    public function __construct()
    {
        for ($i = 1; $i <= 300000; $i++) {
            $division_id = $i % 10;
            $this->data[] = [
                'id' => $i,
                'division_id' => $division_id === 0 ? 10 : $division_id,
                'name' => '社員' . $this->randStr(),
                'age' => mt_rand(20, 60)
            ];
        }
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $chunked_members = array_chunk($this->data, 10000);
        foreach ($chunked_members as $members) {
            // 65535の制限に達しないように、10000件ずつ処理
            Member::insert($members);
        }
    }

    private function randStr(): string
    {
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPUQRSTUVWXYZ';
        return substr(
            str_shuffle($str),
            0,
            10
        );
    }
}
