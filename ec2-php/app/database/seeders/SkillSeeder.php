<?php

namespace Database\Seeders;

use App\Models\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    private $data = [];

    public function __construct()
    {
        $this->data = [
            [
                'id' => 1,
                'name' => 'iOS',
            ],
            [
                'id' => 2,
                'name' => 'Android',
            ],
            [
                'id' => 3,
                'name' => 'Laravel',
            ],
            [
                'id' => 4,
                'name' => 'Vue',
            ],
            [
                'id' => 5,
                'name' => 'CSS',
            ],
            [
                'id' => 6,
                'name' => 'DB',
            ],
            [
                'id' => 7,
                'name' => 'AWS',
            ],
            [
                'id' => 8,
                'name' => 'AI',
            ]
        ];
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            $skill = Skill::find($datum['id']);
            if ($skill) {
                $skill->update($datum);
            } else {
                Skill::create($datum);
            }
        }
    }
}
