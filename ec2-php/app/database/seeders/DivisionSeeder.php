<?php

namespace Database\Seeders;

use App\Models\Division;
use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    private $data = [];

    public function __construct()
    {
        $this->data = [
            [
                'id' => 1,
                'name' => '総務部',
            ],
            [
                'id' => 2,
                'name' => '経理部',
            ],
            [
                'id' => 3,
                'name' => '人事部',
            ],
            [
                'id' => 4,
                'name' => '広報部',
            ],
            [
                'id' => 5,
                'name' => 'マーケティング部',
            ],
            [
                'id' => 6,
                'name' => '受託営業部',
            ],
            [
                'id' => 7,
                'name' => '受託開発部',
            ],
            [
                'id' => 8,
                'name' => '自社製品営業部',
            ],
            [
                'id' => 9,
                'name' => '自社製品開発部',
            ],
            [
                'id' => 10,
                'name' => '技術開発部',
            ],
        ];
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            $division = Division::find($datum['id']);
            if ($division) {
                $division->update($datum);
            } else {
                Division::create($datum);
            }
        }
    }
}
