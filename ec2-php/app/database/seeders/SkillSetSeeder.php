<?php

namespace Database\Seeders;

use App\Models\Skill;
use App\Models\SkillSet;
use Illuminate\Database\Seeder;

class SkillSetSeeder extends Seeder
{
    private $data = [];

    public function __construct()
    {
        // 指定したIDまでの社員に、重複を認めずに2~3個のスキルを紐付ける
        $skill_list = Skill::all()->toArray();
        for ($i = 1; $i <= 300000; $i++) {
            $num_of_skills = mt_rand(2, 3);
            $member_skill_index_list = array_rand($skill_list, $num_of_skills);
            foreach ($member_skill_index_list as $skill_index) {
                $this->data[] = [
                    'member_id' => $i,
                    'skill_id' => $skill_index + 1
                ];
            }
        }
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $chunked_skill_set = array_chunk($this->data, 10000);
        foreach ($chunked_skill_set as $skill_set) {
            // 65535の制限に達しないように、10000件ずつ処理
            SkillSet::insert($skill_set);
        }
    }
}
