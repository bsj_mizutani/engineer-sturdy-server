<?php

namespace Database\Seeders;

use App\Models\FamilyUser;
use Illuminate\Database\Seeder;

class FamilyUserSeeder extends Seeder
{
    private $data = [];

    public function __construct()
    {
        for ($i = 1; $i <= 300000; $i++) {
            $this->data[] = [
                'id' => $i,
                'is_main' => $i % 3 === 2,
                'family_id' => ((int) (($i - 1) / 3)) + 1
            ];
        }
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $chunked_family_users = array_chunk($this->data, 10000);
        foreach ($chunked_family_users as $family_users) {
            // 65535の制限に達しないように、10000件ずつ処理
            FamilyUser::insert($family_users);
        }
    }
}
